<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools"></t:base>

<div class="easyui-layout" style="width:700px;height:400px;">
    <div data-options="region:'center'">
        <t:datagrid checkbox="true" name="dbList" title="目标数据库选择" actionUrl="sqlTaskController.do?dbListGrid"
                    fit="true" fitColumns="true" idField="id" queryMode="group" sortName="description" sortOrder="asc" pagination="false">
            <t:dgCol title="common.id" field="id" sortable="false" ></t:dgCol>
            <t:dgCol title="数据库" sortable="false" field="description" query="true" width="300"></t:dgCol>
        </t:datagrid>
    </div>
</div>
