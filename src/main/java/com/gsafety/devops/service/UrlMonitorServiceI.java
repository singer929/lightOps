package com.gsafety.devops.service;
import com.gsafety.devops.entity.UrlMonitorEntity;
import org.jeecgframework.core.common.service.CommonService;

import java.io.Serializable;

public interface UrlMonitorServiceI extends CommonService{
	
 	public void delete(UrlMonitorEntity entity) throws Exception;
 	
 	public Serializable save(UrlMonitorEntity entity) throws Exception;
 	
 	public void saveOrUpdate(UrlMonitorEntity entity) throws Exception;
 	
}
